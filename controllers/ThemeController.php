<?php

namespace app\controllers;

use Yii;
use app\models\Theme;
use app\models\ThemeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ThemeController implements the CRUD actions for Theme model.
 */
class ThemeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Theme models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ThemeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Theme model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Theme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Theme();

        if ($model->load(Yii::$app->request->post())) {
			try {				
				$picture = UploadedFile::getInstance($model, 'theme_url');
				$model->theme_url = '.'.$picture->extension;
				
				if($model->save()) {
					$model->theme_url = $model->id.'.'.$picture->extension;
					$model->update();
					
					$picture->saveAs('gambar/themes/' . $model->id.'.'.$picture->extension);
					Yii::$app->getSession()->setFlash('success','Data saved!');
					
					return $this->redirect(['view', 'id' => $model->id]);
				} else {
					Yii::$app->getSession()->setFlash('error', 'Data not saved!');
					
					return $this->render('create', [
						'model' => $model,
					]);
				}
			} catch(Exception $e) {
				Yii::$app->getSession()->setFlash('error',"{$e->getMessage()}");
			}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Theme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			try {				
				$picture = UploadedFile::getInstance($model, 'theme_url');
				$model->theme_url = '.'.$picture->extension;
				
				if($model->save()) {
					$model->theme_url = $model->id.'.'.$picture->extension;
					$model->update();
					
					$picture->saveAs('gambar/themes/' . $model->id.'.'.$picture->extension);
					Yii::$app->getSession()->setFlash('success','Data saved!');
					
					return $this->redirect(['view', 'id' => $model->id]);
				} else {
					Yii::$app->getSession()->setFlash('error', 'Data not saved!');
					
					return $this->render('create', [
						'model' => $model,
					]);
				}
			} catch(Exception $e) {
				Yii::$app->getSession()->setFlash('error',"{$e->getMessage()}");
			}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Theme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $thisNow = $this;
        
        if(Yii::$app->request->get('id')) {
			unlink(getcwd().'/gambar/themes/'.$model->theme_url);
			
			$thisNow->findModel($id)->delete();
			
			return $thisNow->redirect(['index']);
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
    }

    /**
     * Finds the Theme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Theme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Theme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
