<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "background".
 *
 * @property integer $id
 * @property string $background_name
 * @property string $background_url
 */
class Background extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'background';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['background_name', 'background_url'], 'required'],
            [['background_name'], 'string', 'max' => 10],
            [['background_url'], 'file', 'extensions' => 'jpg,jpeg,png' ,'maxSize' => 1024 * 1024 *1, 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'background_name' => 'Background Name',
            'background_url' => 'Background Url',
        ];
    }
}
