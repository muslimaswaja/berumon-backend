<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "board".
 *
 * @property integer $id
 * @property integer $creator_id
 * @property integer $created_id
 * @property string $board_name
 * @property string $description
 * @property string $cover_url
 *
 * @property Groups $created
 * @property Users $creator
 */
class Board extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'board';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'created_id', 'board_name', 'description', 'cover_url'], 'required'],
            [['creator_id', 'created_id'], 'integer'],
            [['board_name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 500],
            [['cover_url'], 'file', 'extensions' => 'jpg,jpeg,png' ,'maxSize' => 1024 * 1024 *1, 'on' => 'create'],
            [['created_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['created_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_id' => 'Creator ID',
            'created_id' => 'Created ID',
            'board_name' => 'Board Name',
            'description' => 'Description',
            'cover_url' => 'Cover Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreated()
    {
        return $this->hasOne(Groups::className(), ['id' => 'created_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator_id']);
    }
}
