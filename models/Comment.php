<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $id_snap
 * @property integer $id_commentator
 * @property string $comment
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_snap', 'id_commentator', 'comment'], 'required'],
            [['id_snap', 'id_commentator'], 'integer'],
            [['comment'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_snap' => 'Id Snap',
            'id_commentator' => 'Id Commentator',
            'comment' => 'Comment',
        ];
    }
}
