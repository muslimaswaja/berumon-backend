<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $group_name
 * @property string $group_cover_url
 * @property string $group_description
 *
 * @property Board[] $boards
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'group_name', 'group_cover_url', 'group_description'], 'required'],
            [['category_id'], 'integer'],
            [['group_name'], 'string', 'max' => 50],
            [['group_cover_url'], 'file', 'extensions' => 'jpg,jpeg,png' ,'maxSize' => 1024 * 1024 *1, 'on' => 'create'],
            [['group_description'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'group_name' => 'Group Name',
            'group_cover_url' => 'Group Cover Url',
            'group_description' => 'Group Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoards()
    {
        return $this->hasMany(Board::className(), ['created_id' => 'id']);
    }
}
