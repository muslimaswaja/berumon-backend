<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $id_sender
 * @property integer $id_receiver
 * @property string $time_sent
 * @property string $message
 *
 * @property Users $idSender
 */
class Messages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sender', 'id_receiver', 'time_sent', 'message'], 'required'],
            [['id_sender', 'id_receiver'], 'integer'],
            [['time_sent'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 500],
            [['id_sender'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_sender' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sender' => 'Id Sender',
            'id_receiver' => 'Id Receiver',
            'time_sent' => 'Time Sent',
            'message' => 'Message',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSender()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_sender']);
    }
}
