<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $default_privileges
 * @property integer $message_on
 * @property integer $friend_snap_on
 * @property integer $group_on
 * @property integer $id_sound
 * @property integer $id_theme
 * @property integer $id_background
 *
 * @property Users $idUser
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'default_privileges', 'message_on', 'friend_snap_on', 'group_on', 'id_sound', 'id_theme', 'id_background'], 'required'],
            [['id_user', 'message_on', 'friend_snap_on', 'group_on', 'id_sound', 'id_theme', 'id_background'], 'integer'],
            [['default_privileges'], 'string', 'max' => 15],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'default_privileges' => 'Default Privileges',
            'message_on' => 'Message On',
            'friend_snap_on' => 'Friend Snap On',
            'group_on' => 'Group On',
            'id_sound' => 'Id Sound',
            'id_theme' => 'Id Theme',
            'id_background' => 'Id Background',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }
}
