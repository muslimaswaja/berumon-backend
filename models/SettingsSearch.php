<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Settings;

/**
 * SettingsSearch represents the model behind the search form about `app\models\Settings`.
 */
class SettingsSearch extends Settings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'message_on', 'friend_snap_on', 'group_on', 'id_sound', 'id_theme', 'id_background'], 'integer'],
            [['default_privileges'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Settings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'message_on' => $this->message_on,
            'friend_snap_on' => $this->friend_snap_on,
            'group_on' => $this->group_on,
            'id_sound' => $this->id_sound,
            'id_theme' => $this->id_theme,
            'id_background' => $this->id_background,
        ]);

        $query->andFilterWhere(['like', 'default_privileges', $this->default_privileges]);

        return $dataProvider;
    }
}
