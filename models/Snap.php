<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "snap".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $caption
 * @property string $url
 * @property string $image_url
 * @property integer $id_board
 * @property string $post_time
 *
 * @property Users $idUser
 */
class Snap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'snap';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'caption', 'url', 'image_url', 'id_board', 'post_time'], 'required'],
            [['id_user', 'id_board'], 'integer'],
            [['caption'], 'string', 'max' => 500],
            [['url'], 'string', 'max' => 200],
            [['image_url'], 'file', 'extensions' => 'jpg,jpeg,png' ,'maxSize' => 1024 * 1024 *1, 'on' => 'create'],
            [['post_time'], 'string', 'max' => 100],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'caption' => 'Caption',
            'url' => 'Url',
            'image_url' => 'Image Url',
            'id_board' => 'Id Board',
            'post_time' => 'Post Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }
}
