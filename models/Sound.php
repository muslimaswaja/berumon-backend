<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sound".
 *
 * @property integer $id
 * @property string $sound_name
 * @property string $sound_url
 */
class Sound extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sound';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sound_name', 'sound_url'], 'required'],
            [['sound_name'], 'string', 'max' => 10],
            [['sound_url'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sound_name' => 'Sound Name',
            'sound_url' => 'Sound Url',
        ];
    }
}
