<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $email
 * @property string $password
 * @property string $biodata
 * @property string $location
 * @property string $photo
 *
 * @property Board[] $boards
 * @property Messages[] $messages
 * @property Settings[] $settings
 * @property Snap[] $snaps
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'email', 'password', 'biodata', 'location', 'photo'], 'required'],
            [['user_name', 'location'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 128],
            [['biodata'], 'string', 'max' => 500],
            [['photo'], 'file', 'extensions' => 'jpg,jpeg,png' ,'maxSize' => 1024 * 1024 *1, 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_name' => 'User Name',
            'email' => 'Email',
            'password' => 'Password',
            'biodata' => 'Biodata',
            'location' => 'Location',
            'photo' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoards()
    {
        return $this->hasMany(Board::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['id_sender' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(Settings::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSnaps()
    {
        return $this->hasMany(Snap::className(), ['id_user' => 'id']);
    }
}
