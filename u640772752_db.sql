
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 20, 2017 at 02:02 AM
-- Server version: 10.1.20-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u640772752_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `background`
--

CREATE TABLE IF NOT EXISTS `background` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `background_name` varchar(10) NOT NULL,
  `background_url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `background`
--

INSERT INTO `background` (`id`, `background_name`, `background_url`) VALUES
(1, 'Lorem', '1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `creator_id` int(4) NOT NULL,
  `created_id` int(4) NOT NULL,
  `board_name` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `cover_url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `created_id` (`created_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`id`, `creator_id`, `created_id`, `board_name`, `description`, `cover_url`) VALUES
(1, 1, 1, 'public', 'Snap yang di-share ke publik.', ''),
(2, 2, 1, 'public', 'Snap yang di-share ke publik.', '');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`) VALUES
(1, 'Kain & Bahan'),
(2, 'Batik & Tenun'),
(3, 'Dress'),
(4, 'Baju Muslim'),
(5, 'Kemeja'),
(6, 'Kaos'),
(7, 'Jeans'),
(8, 'Rok'),
(9, 'Celana'),
(10, 'Topi'),
(11, 'Sepatu & Sandal'),
(12, 'personal');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `id_snap` int(4) NOT NULL,
  `id_commentator` int(4) NOT NULL,
  `comment` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_snap` (`id_snap`),
  KEY `id_commentator` (`id_commentator`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE IF NOT EXISTS `follow` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `id_follower` int(4) NOT NULL,
  `id_followed` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_follower` (`id_follower`),
  KEY `id_followed` (`id_followed`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `category_id` int(4) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `group_cover_url` varchar(200) NOT NULL,
  `group_description` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `category_id`, `group_name`, `group_cover_url`, `group_description`) VALUES
(1, 12, 'personal', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `id_sender` int(4) NOT NULL,
  `id_receiver` int(4) NOT NULL,
  `time_sent` varchar(50) NOT NULL,
  `message` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_sender` (`id_sender`),
  KEY `id_receiver` (`id_receiver`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `id_user` int(4) NOT NULL,
  `default_privileges` varchar(15) NOT NULL,
  `message_on` int(1) NOT NULL,
  `friend_snap_on` int(1) NOT NULL,
  `group_on` int(1) NOT NULL,
  `id_sound` int(2) NOT NULL,
  `id_theme` int(2) NOT NULL,
  `id_background` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_setting` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `snap`
--

CREATE TABLE IF NOT EXISTS `snap` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `id_user` int(4) NOT NULL,
  `caption` varchar(500) NOT NULL,
  `url` varchar(200) NOT NULL,
  `image_url` varchar(200) NOT NULL,
  `id_board` int(4) NOT NULL,
  `post_time` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_board` (`id_board`),
  KEY `id_board_2` (`id_board`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `snap`
--

INSERT INTO `snap` (`id`, `id_user`, `caption`, `url`, `image_url`, `id_board`, `post_time`) VALUES
(1, 1, 'No caption', 'http://www.aweawe.com/', '1.png', 1, '3rfe'),
(2, 1, 'No comment.', 'http://file-manager.idhostinger.com/6/index.php', '2.png', 1, '12.34');

-- --------------------------------------------------------

--
-- Table structure for table `sound`
--

CREATE TABLE IF NOT EXISTS `sound` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `sound_name` varchar(10) NOT NULL,
  `sound_url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `theme_name` varchar(10) NOT NULL,
  `theme_url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `theme`
--

INSERT INTO `theme` (`id`, `theme_name`, `theme_url`) VALUES
(1, 'Contoh', '1.jpg'),
(2, 'Nothing', '2.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `biodata` varchar(500) NOT NULL,
  `location` varchar(20) NOT NULL,
  `photo` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `email`, `password`, `biodata`, `location`, `photo`) VALUES
(1, 'Dianpelangi', 'dianpelangi@gmail.com', '01dianpel02', 'Violet itu biru, mawar itu merah', 'Jakarta', '1.jpg'),
(2, 'JuanWahyu', 'Ju.wahyu@gmail.com', 'wahyu190', 'Batik Pekalongan --- Dagadu Bali', 'Sanur Beach', '2.jpg'),
(60, 'syaifululum', 'bang@ipul.com', 'lalala', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(64, 'dano', 'da@gmail.come', '13a5d51391f6a6ff5a94394b0dee6a35bf66fd73', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(65, 'muslim', 'muslimaswaja@yahoo.com', 'e170f80139aac716ebca4320121de416231b4109', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(66, 'muslimp', 'po@p.m', '516b9783fca517eecbd1d064da2d165310b19759', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(67, 'rosi', 'rosi@gmail.com', 'e19ed4e7cfe9a76b0c96dd1059493c2eb1a4a330', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(68, 'musang', 'mus@xyz.com', 'c806211ae00e06753a212e70848f3aeca753e3b7', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(69, 'nisak', 'desti@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(42, 'musmus', 'musmus@gmail.com', 'hoho', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(41, 'nisa', 'nisa@gmail.com', 'nisa', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', ''),
(40, 'da', 'da@gmail.com', 'da', 'Biodata Anda di sini. Ubah di pengaturan', 'Kota Anda', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
