<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            
            <?php if(!Yii::$app->user->isGuest) { ?>
            <div class="pull-left info">
                <p>Administrator</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Access granted</a>
            </div>
            <?php } ?>
            
            <?php if(Yii::$app->user->isGuest) { ?>
            <div class="pull-left info">
                <p>Guest</p>

                <a href="#"><i class="fa fa-circle text-danger"></i> Access not granted</a>
            </div>
            <?php } ?>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Login', 'icon' => 'fa fa-sign-in', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest,],
                    [
                        'label' => 'Administrasi',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Background', 'url' => ['/background'],],
                            ['label' => 'Board', 'url' => ['/board'],],
                            ['label' => 'Category', 'url' => ['/category'],],
                            ['label' => 'Comment', 'url' => ['/comment'],],
                            ['label' => 'Groups', 'url' => ['/groups'],],
                            ['label' => 'Messages', 'url' => ['/messages'],],
                            ['label' => 'Settings', 'url' => ['/settings'],],
                            ['label' => 'Snap', 'url' => ['/snap'],],
                            ['label' => 'Sound', 'url' => ['/sound'],],
                            ['label' => 'Theme', 'url' => ['/theme'],],
                            ['label' => 'Users', 'url' => ['/users'],],
                            /*[
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],*/
                        ],
                    ],
                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'], 'visible' => !Yii::$app->user->isGuest,],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'], 'visible' => !Yii::$app->user->isGuest,],
                ],
            ]
        ) ?>

    </section>

</aside>
