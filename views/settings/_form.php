<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user')->textInput() ?>

    <?= $form->field($model, 'default_privileges')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message_on')->textInput() ?>

    <?= $form->field($model, 'friend_snap_on')->textInput() ?>

    <?= $form->field($model, 'group_on')->textInput() ?>

    <?= $form->field($model, 'id_sound')->textInput() ?>

    <?= $form->field($model, 'id_theme')->textInput() ?>

    <?= $form->field($model, 'id_background')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
