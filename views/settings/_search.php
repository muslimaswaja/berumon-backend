<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SettingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'default_privileges') ?>

    <?= $form->field($model, 'message_on') ?>

    <?= $form->field($model, 'friend_snap_on') ?>

    <?php // echo $form->field($model, 'group_on') ?>

    <?php // echo $form->field($model, 'id_sound') ?>

    <?php // echo $form->field($model, 'id_theme') ?>

    <?php // echo $form->field($model, 'id_background') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
