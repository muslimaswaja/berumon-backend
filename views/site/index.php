<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Web Admin Berumon';
?>
<div class="site-index">

    <div class="jumbotron">
		<img src="logoberumon.png" style="width: 30%;height: auto">
		
		<br><br>
		
        <h1>Benang Jarum Online</h1>
        
        <?php if(Yii::$app->user->isGuest) { ?>
        <p class="lead">Silahkan login terlebih dahulu untuk mendapatkan hak akses administrator.</p>

        <p><?= Html::a(
			'Login Administrator',
			['/site/login'],
			['data-method' => 'post', 'class' => 'btn btn-lg btn-success']
		) ?></p>
		<?php } ?>
		
		<?php if(!Yii::$app->user->isGuest) { ?>
        <p class="lead">Selamat datang! Semoga harimu menyenangkan! :-)</p>
		<?php } ?>
    </div>

</div>
