<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SnapSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="snap-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'caption') ?>

    <?= $form->field($model, 'url') ?>

    <?= $form->field($model, 'image_url') ?>

    <?php // echo $form->field($model, 'id_board') ?>

    <?php // echo $form->field($model, 'post_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
