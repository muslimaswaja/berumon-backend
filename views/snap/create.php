<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Snap */

$this->title = 'Create Snap';
$this->params['breadcrumbs'][] = ['label' => 'Snaps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="snap-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
