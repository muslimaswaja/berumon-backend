<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SnapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Snaps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="snap-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Snap', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_user',
            'caption',
            'url:url',
            'image_url:url',
            // 'id_board',
            // 'post_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
