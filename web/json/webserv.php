<?php
// Mendefinisikan tipe JSON
header("Content-Type: application/json; charset=UTF-8");

// Memberikan kontrol akses eksternal
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With');
header('Access-Control-Allow-Methods: GET, PUT, POST');
?>